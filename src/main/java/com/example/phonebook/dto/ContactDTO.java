package com.example.phonebook.dto;


import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.Size;
import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class ContactDTO {

    @NonNull
    private String name;

    @NonNull
    @Size(min = 10 ,max = 12)
    private String phoneNumber;

    @NonNull
    @Email
    private String email;

    @NonNull
    private String Address;
}
