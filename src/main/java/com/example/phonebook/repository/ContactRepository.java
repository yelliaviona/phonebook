package com.example.phonebook.repository;

import com.example.phonebook.domain.Contact;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface ContactRepository extends JpaRepository<Contact, Long> {

    @Query(value = "SELECT * FROM contact WHERE LOWER(name) LIKE %:search% " +
            "OR phone_number LIKE %:search% ", nativeQuery = true)
    List<Contact> searchContacts(@Param("search") String search);

    Optional<Contact> findByPhoneNumber(String phoneNumber);
}
