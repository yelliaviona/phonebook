package com.example.phonebook.controller;

import com.example.phonebook.dto.ContactDTO;
import com.example.phonebook.exception.GeneralBodyResponse;
import com.example.phonebook.service.ContactService;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;


@Controller
@RequestMapping("/contacts")
@Slf4j
public class ContactController {

    private final ContactService contactService;

    /**
     * Constructs a new ContactController with the provided ContactService.
     *
     * @param contactService the ContactService to be used by this controller
     */
    public ContactController(ContactService contactService) {
        this.contactService = contactService;
    }

    /**
     * Displays the main Phonebook page.
     *
     * @return the name of the HTML file for the Phonebook page
     */
    @GetMapping("/")
    public String showPhonebook() {
        return "index.html";
    }

    /**
     * Adds a new contact to the Phonebook.
     *
     * @param request the ContactDTO containing the details of the new contact
     * @return ResponseEntity containing a GeneralBodyResponse with status and message
     */
    @PostMapping()
    public ResponseEntity<?> addContact(@RequestBody @Validated ContactDTO request) {
        GeneralBodyResponse response = contactService.addContact(request);
        return ResponseEntity
                .ok()
                .body(response);
    }

    /**
     * Retrieves a contact by its ID.
     *
     * @param id the ID of the contact to retrieve
     * @return ResponseEntity containing a GeneralBodyResponse with status and message
     */
    @GetMapping("/{id}")
    public ResponseEntity<?> getContactById(@PathVariable Long id) {
        GeneralBodyResponse response = contactService.getContactById(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    /**
     * Retrieves all contacts from the Phonebook.
     *
     * @return ResponseEntity containing a GeneralBodyResponse with status and message
     */
    @GetMapping("/getAll")
    public ResponseEntity<?> getAllContacts() {
        GeneralBodyResponse response = contactService.getAllContacts();
        return ResponseEntity
                .ok()
                .body(response);
    }

    /**
     * Updates an existing contact in the Phonebook.
     *
     * @param id      the ID of the contact to update
     * @param request the ContactDTO containing the updated details
     * @return ResponseEntity containing a GeneralBodyResponse with status and message
     */
    @PutMapping("/{id}")
    public ResponseEntity<?> updateContact(@PathVariable Long id, @RequestBody ContactDTO request) {
        GeneralBodyResponse response = contactService.updateContact(id, request);
        return ResponseEntity
                .ok()
                .body(response);
    }

    /**
     * Deletes a contact from the Phonebook by its ID.
     *
     * @param id the ID of the contact to delete
     * @return ResponseEntity containing a GeneralBodyResponse with status and message
     */
    @DeleteMapping("/{id}")
    public ResponseEntity<?> deleteContact(@PathVariable Long id) {
        GeneralBodyResponse response = contactService.deleteContact(id);
        return ResponseEntity
                .ok()
                .body(response);
    }

    /**
     * Searches for contacts in the Phonebook by name or phone number.
     *
     * @param search the search query for finding contacts
     * @return ResponseEntity containing a GeneralBodyResponse with status and message
     */
    @GetMapping("/search")
    public ResponseEntity<?> searchContact(@RequestParam String search) {
        GeneralBodyResponse response = contactService.searchContacts(search);
        return ResponseEntity
                .ok()
                .body(response);
    }
}