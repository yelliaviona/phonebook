package com.example.phonebook.exception;

import lombok.Getter;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@Getter
@ResponseStatus(value = HttpStatus.BAD_REQUEST)
public class CustomBadRequestException extends RuntimeException {
    private final int code;
    private final String status;
    private final String message;

    public CustomBadRequestException(int code, String status, String message) {
        super();

        this.code = code;
        this.status = status;
        this.message = message;
    }

}
