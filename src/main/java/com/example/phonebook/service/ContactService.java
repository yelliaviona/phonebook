package com.example.phonebook.service;

import com.example.phonebook.domain.Contact;
import com.example.phonebook.dto.ContactDTO;
import com.example.phonebook.exception.CustomNotFoundException;
import com.example.phonebook.exception.GeneralBodyResponse;
import com.example.phonebook.repository.ContactRepository;
import jakarta.transaction.Transactional;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

/**
 * Service class for managing contacts in the Phonebook application.
 */
@Service
@Transactional
public class ContactService {

    private final ContactRepository contactRepository;
    /**
     * Constructor for ContactService.
     * @param contactRepository The repository for managing Contact entities.
     */

    public ContactService(ContactRepository contactRepository) {
        this.contactRepository = contactRepository;
    }

    /**
     * Adds a new contact to the Phonebook.
     * @param request The contact information to be added.
     * @return GeneralBodyResponse containing the result of the operation.
     * @throws CustomNotFoundException if the phone number already exists.
     */
    public GeneralBodyResponse addContact(ContactDTO request) {

        //check phoneNumber
        Optional<Contact> checkPhoneNumber = contactRepository.findByPhoneNumber(request.getPhoneNumber());
        if (checkPhoneNumber.isPresent()) {
            throw new CustomNotFoundException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Phone Number " + request.getPhoneNumber() + " already exist");
        }

        Contact contact = new Contact();
        contact.setName(request.getName());
        contact.setEmail(request.getEmail());
        contact.setPhoneNumber(request.getPhoneNumber());
        contact.setAddress(request.getAddress());
        contact.setCreatedAt(LocalDateTime.now());
        contactRepository.save(contact);
        return new GeneralBodyResponse(201,HttpStatus.CREATED.getReasonPhrase(),"Successfully created",contact);
    }

    /**
     * Retrieves a contact by ID.
     * @param id The ID of the contact to retrieve.
     * @return GeneralBodyResponse containing the retrieved contact.
     * @throws CustomNotFoundException if the contact is not found.
     */
    public GeneralBodyResponse getContactById(Long id) {
        Contact contact = contactRepository.findById(id)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Contact not found with ID: " + id));
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(),"Contact fetched successfully", contact);
    }

    /**
     * Retrieves all contacts in the Phonebook.
     * @return GeneralBodyResponse containing the list of contacts.
     * @throws CustomNotFoundException if no contacts are found.
     */
    public GeneralBodyResponse getAllContacts() {
        List<Contact> contacts = contactRepository.findAll();
        if (contacts.isEmpty()){
            throw new CustomNotFoundException(404,HttpStatus.NOT_FOUND.getReasonPhrase(), "No Contacts Found.");
        }
        return new GeneralBodyResponse(200,HttpStatus.OK.getReasonPhrase(), "contacts list retrieved successfully",contacts);
    }

    /**
     * Updates an existing contact in the Phonebook.
     * @param contactId The ID of the contact to update.
     * @param request The updated contact information.
     * @return GeneralBodyResponse containing the result of the update operation.
     * @throws CustomNotFoundException if the contact is not found or the new phone number already exists.
     */
    public GeneralBodyResponse updateContact(Long contactId, ContactDTO request) {
        // Fetch the existing contact from the database
        Contact existingContact = contactRepository.findById(contactId)
                .orElseThrow(() -> new CustomNotFoundException(404, HttpStatus.NOT_FOUND.getReasonPhrase(), "Contact not found with ID: " + contactId));

        // Check if the phone number is being changed
        if (!existingContact.getPhoneNumber().equals(request.getPhoneNumber())) {
            // Check if the new phone number already exists in the database
            Optional<Contact> existingContactWithNewPhoneNumber = contactRepository.findByPhoneNumber(request.getPhoneNumber());
            if (existingContactWithNewPhoneNumber.isPresent()) {
                throw new CustomNotFoundException(400, HttpStatus.BAD_REQUEST.getReasonPhrase(), "Phone Number " + request.getPhoneNumber() + " already exists");
            }
        }
        if (request.getName()!=null){
            existingContact.setName(request.getName());
        }
        if (request.getPhoneNumber() != null) {
            existingContact.setPhoneNumber(request.getPhoneNumber());
        }
        if (request.getEmail() != null) {
            existingContact.setEmail(request.getEmail());
        }
        if (request.getAddress() != null) {
            existingContact.setAddress(request.getAddress());
        }

        Contact savedContact = contactRepository.save(existingContact);

        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Successfully updated contact", savedContact);
    }

    /**
     * Deletes a contact from the Phonebook.
     * @param id The ID of the contact to delete.
     * @return GeneralBodyResponse containing the result of the delete operation.
     * @throws CustomNotFoundException if the contact is not found.
     */
    public GeneralBodyResponse deleteContact(Long id) {
        Contact contact = contactRepository.findById(id)
                        .orElseThrow(() -> new CustomNotFoundException(404,HttpStatus.NOT_FOUND.getReasonPhrase(), "Contact not found with id :"+id));
        contactRepository.deleteById(id);
        return new GeneralBodyResponse(200, HttpStatus.OK.getReasonPhrase(), "Contact details retrieved successfully", contact);


    }

    /**
     * Searches contacts in the Phonebook by name or number.
     * @param query The search query (name or number).
     * @return GeneralBodyResponse containing the list of matching contacts.
     * @throws CustomNotFoundException if no matching contacts are found.
     */
    public GeneralBodyResponse searchContacts(String query) {
        List<Contact> contacts= new ArrayList<>();
        contacts = contactRepository.searchContacts(query);
        if (contacts.isEmpty()){
            throw  new CustomNotFoundException(404,HttpStatus.NOT_FOUND.getReasonPhrase(), "Search by name or number not found ");
        }
        return new GeneralBodyResponse(200,HttpStatus.OK.getReasonPhrase(),"Successfully search contact",contacts);
    }
}